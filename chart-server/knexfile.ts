import dotenv from "dotenv";
dotenv.config();
// Update with your config settings.

let configs = {
  development: {
    client: "postgresql",
    connection: {
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
};

module.exports = configs;
export default configs;
