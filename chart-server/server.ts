import express from 'express';
import dotenv from 'dotenv';
import fs from 'fs';
import http from 'http';
import cors from 'cors';
let env = dotenv.parse(fs.readFileSync('.env').toString());
import WebSocket from 'ws';

// let app = express();
// app.use(cors());
// let server = http.createServer(app);
// let wss = new WebSocket.Server({ server });

const wss = new WebSocket.Server({ port: 8200 });

wss.on('connection', (ws) => {
  console.log('got ws connection:');
  setInterval(() => {
    ws.send(
      JSON.stringify({ label: Date.now(), value: Math.random() * 10 + 20 }),
    );
  }, 100);
  ws.on('message', (message) => {
    console.log('received ws msg:', message);
  });
});

// app.get('/', (req, res) => res.send('Hello World'));

// app.listen(env.PORT, () => console.log('listening on port ' + env.PORT));
