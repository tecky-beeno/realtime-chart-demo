import React, { useState, useEffect } from 'react';
import './App.css';
import { Line, ChartData } from 'react-chartjs-2';
import jsonData from './ohlc.json';
// console.log(jsonData);

// fetch('http://localhost:8200')
//   .then((res) => res.text())
//   .then((body) => {
//     console.log({ body });
//   });

let ws = new WebSocket('ws://localhost:8200');
type DataFrame = {
  label: string;
  value: number;
};
function App() {
  const [dataSet, setDataSet] = useState([] as DataFrame[]);
  useEffect(() => {
    function handleMessage(message: any) {
      let data = JSON.parse(message.data);
      let newDataSet = [...dataSet, data];
      setDataSet(newDataSet.slice(-300));
    }
    ws.addEventListener('message', handleMessage);
    return () => {
      ws.removeEventListener('message', handleMessage);
    };
  }, [setDataSet, dataSet]);
  useEffect(() => {
    if ('dev') {
      return;
    }
    fetch('https://api.cryptowat.ch/markets/kraken/btcusd/ohlc?periods=60')
      .then((res) => res.json())
      .catch((err) => jsonData)
      .then((data) => {
        let dataset = data.result[60].map((data: any) => {
          return {
            label: data[0] * 1000,
            value: data[1],
          };
        });
        setDataSet(dataset);
      });
  }, []);
  return (
    <div className="App1">
      <header className="App-header1">
        <Line
          data={{
            labels: dataSet.map((entry) => new Date(entry.label).toString()),
            datasets: [
              {
                label: 'opening',
                data: dataSet.map((entry) => entry.value),
                fill: false,
              },
            ],
          }}
        ></Line>
      </header>
    </div>
  );
}

export default App;
